﻿using System;
using Newtonsoft.Json;
namespace TestJson.Net
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = new person
            {
                Age=18,
                Email="45182352@qq.com",
                Height="173cm",
                Name="youthcool",
                Sex="男",
                //Tel="110"


            };
            
            Console.WriteLine(JsonConvert.SerializeObject(result));
            Console.ReadLine();
        }
    }

    public class person
    {
        /// <summary>
        /// 替换Name属性成中文
        /// </summary>
        [JsonProperty("熊", Order = 5)]
        public string Name { set; get; }        
        /// <summary>
        /// 必须有值，
        /// </summary>
        [JsonProperty(Required = Required.Always, Order = 4)]
        public string Sex { set; get; }
        /// <summary>
        /// 没有值，序列化结果中属性忽略
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore,Order = 2)]
        public string Email { set; get; }
        /// <summary>
        ///  没有值，序列化结果中属性任显示，值为null
        /// </summary>
        [JsonProperty(Required = Required.AllowNull,Order =2)]
        public string Tel { set; get; }
        /// <summary>
        /// 排序，未指定Order序号的属性，界定于大于负数小于正数，并按默认顺序排序
        /// </summary>
        [JsonProperty(Order = -2)]
        public string Height { set; get; }

        [JsonProperty(Order = 0)]
        public int Age { set; get; }

        [JsonProperty(Order = -1)]
        public string profession { set; get; }
    }
}
